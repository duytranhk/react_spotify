import { Component } from '@angular/core';
import { fadeInOut } from '../../services/animation.service';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less'],
  animations: [fadeInOut]
})
export class DashboardComponent {
  public selectedTrackId: string;
  constructor() { }

  public selectTrackEvent(trackId: string): void {
    this.selectedTrackId = trackId;
  }
}
