import { Component, OnInit } from '@angular/core';
import { fadeInOut } from '../../services/animation.service';
import { UserLogin } from 'src/app/models/login.model';
import { NgForm } from '@angular/forms';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less'],
  animations: [fadeInOut]
})
export class LoginComponent implements OnInit {
  constructor(private authService: AuthService, private router: Router) { }
  public userLogin: UserLogin = new UserLogin();

  ngOnInit(): void {
    const self = this;
    if (self.authService.isLoggedIn) {
      self.router.navigate(['']);
    }
  }

  // view events
  public onSubmitLogin(form: NgForm): void {
    const self = this;
    if (form.valid) {
      self.authService.login(self.userLogin.email, self.userLogin.password).then(token => {
        self.router.navigate(['']);
      });
    }
  }

  public onSpotifyLogin(): void {
    const self = this;
    self.authService.spotifyLogin();
  }
}
