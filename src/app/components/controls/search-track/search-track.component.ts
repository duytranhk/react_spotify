import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { SpotifyService } from 'src/app/services/spotify.service';
import { SpotifyTrackDetailModel } from 'src/app/models/spotify.model';

@Component({
    selector: 'search-track',
    templateUrl: './search-track.component.html',
    styleUrls: ['./search-track.component.less']
})
export class SearchTrackComponent implements OnInit {
    @Output() selectTrack = new EventEmitter<string>();
    public searchValue: string;
    public trackLists: SpotifyTrackDetailModel[];
    constructor(private spotifyService: SpotifyService) {
    }

    ngOnInit(): void {
        const self = this;
    }

    // view events
    onTrackClick(id: string): void {
        this.selectTrack.emit(id);
    }

    onClickSearch(): void {
        const self = this;
        self.spotifyService.searchTracks(self.searchValue).then(resp => {
            self.trackLists = resp.tracks.items;
            console.log(self.trackLists);
        });
    }
}
