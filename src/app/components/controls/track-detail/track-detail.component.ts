import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { SpotifyService } from 'src/app/services/spotify.service';
import { SpotifyTrackDetailModel } from 'src/app/models/spotify.model';

@Component({
    selector: 'track-detail',
    templateUrl: './track-detail.component.html',
    styleUrls: ['./track-detail.component.less']
})
export class TrackDetailComponent implements OnChanges {
    @Input() trackId: string;
    public trackDetail: SpotifyTrackDetailModel;
    constructor(private spotifyService: SpotifyService) {
    }

    ngOnChanges(changes: SimpleChanges): void {
        const self = this;
        if (changes.trackId.currentValue !== changes.trackId.previousValue) {
            self.spotifyService.getTrack(self.trackId).then(resp => {
                self.trackDetail = resp;
                console.log(self.trackDetail);
            });
        }
    }

    // view events
}
