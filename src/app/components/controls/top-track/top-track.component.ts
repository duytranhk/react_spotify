import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { SpotifyService } from 'src/app/services/spotify.service';
import { SpotifyPlaylistModel, SpotifyTrackModel } from 'src/app/models/spotify.model';

@Component({
    selector: 'top-track',
    templateUrl: './top-track.component.html',
    styleUrls: ['./top-track.component.less']
})
export class TopTrackComponent implements OnInit {
    @Output() selectTrack = new EventEmitter<string>();

    public playlistModel: SpotifyPlaylistModel = {};
    public get trackLists(): SpotifyTrackModel[] {
        if (!this.playlistModel) { return []; }
        return this.playlistModel.tracks.items.filter((item, index) => index < 5);
    }

    constructor(private spotifyService: SpotifyService) {
    }

    ngOnInit(): void {
        const self = this;
        self.spotifyService.getAustraliaPlaylist().then(resp => {
            this.playlistModel = resp;
        });
    }

    // view events
    onTrackClick(id: string): void {
        this.selectTrack.emit(id);
    }
}
