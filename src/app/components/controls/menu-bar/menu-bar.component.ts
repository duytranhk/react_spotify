import { Component } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'menu-bar',
  templateUrl: './menu-bar.component.html',
  styleUrls: ['./menu-bar.component.less']
})
export class MenuBarComponent {
  constructor(private authService: AuthService, private router: Router) {
  }

  // view events
  public onSignOutClick(): void {
    const self = this;
    self.authService.logout();
    self.router.navigate(['login']);
  }
}
