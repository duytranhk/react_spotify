import { Component } from '@angular/core';
import { fadeInOut } from '../../services/animation.service';

@Component({
  selector: 'not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.less'],
  animations: [fadeInOut]
})
export class NotFoundComponent {
  constructor() {}
}
