import { Component } from '@angular/core';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  public get showMenuBar(): boolean {
    return this.authService.isLoggedIn;
  }

  constructor(private authService: AuthService) { }
}
