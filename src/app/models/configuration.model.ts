export interface UserCredential {
  email: string;
  password: string;
  token: string;
}

export interface SpotifyConfig {
  clientId: string;
  responseType: string;
  authUrl: string;
  apiUrl: string;
}
