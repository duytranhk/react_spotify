export interface SpotifyPlaylistModel {
    id?: string;
    name?: string;
    collaborative?: boolean;
    description?: string;
    href?: string;
    tracks?: SpotifyPlaylistTrackModel;
    images?: SpotifyImageModel[];
}

export interface SpotifyPlaylistTrackModel {
    href: string;
    limit: number;
    total: number;
    ofset: number;
    previous: number;
    items: SpotifyTrackModel[];
}

export interface SpotifyTrackModel {
    added_at: Date;
    is_local: boolean;
    track: SpotifyTrackDetailModel;
}

export interface SpotifyTrackDetailModel {
    id: string;
    name: string;
    href: string;
    duration_ms: number;
    preview_url: string;
    popularity: number;
    artists: SpotifyArtstModel[];
}

export interface SpotifyArtstModel {
    id: string;
    name: string;
    href: string;
    type: string;
}

export interface SpotifyImageModel {
    height: number;
    url: string;
    width: number;
}

export interface SpotifySearchResult<T> {
    href: string;
    items: T[];
    total: number;
    limit: number;
    offset: number;
}

export interface SpotifyTrackSearchResult {
    tracks: SpotifySearchResult<SpotifyTrackDetailModel>;
}
