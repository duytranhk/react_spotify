import { Injectable } from '@angular/core';

@Injectable()
export class EventName {
    public static SELECT_TRACK = '@track:select';
}
