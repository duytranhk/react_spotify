import { Injectable } from '@angular/core';
import { StorageKey } from '../constants/storage-key.const';
import { StorageManagerService } from './storage-manager.service';
import { AuthEndpoint } from './endpoints/auth-endpoint.service';
import { ConfigurationService } from './configuration.service';
import { Utilities } from './utilities.service';

@Injectable()
export class AuthService {
  constructor(
    private storageManager: StorageManagerService,
    private authEndpoint: AuthEndpoint,
    private config: ConfigurationService,
  ) { }

  get isLoggedIn(): boolean {
    return this.storageManager.exists(StorageKey.ACCESS_TOKEN);
  }

  public login(email: string, password: string): Promise<string> {
    if (this.isLoggedIn) {
      this.logout();
    }

    return this.authEndpoint.getLoginEndpoint(email, password).then(token => this.processLoginResponse(token, ''));
  }

  public spotifyLogin() {
    const self = this;
    window.location.replace(
      self.config.spotifyConfig.authUrl +
      `?client_id=${self.config.spotifyConfig.clientId}` +
      `&redirect_uri=${encodeURI(self.config.baseUrl)}` +
      `&response_type=${self.config.spotifyConfig.responseType}`
    );
  }

  public validateUrl(url: string): boolean {
    const self = this;
    if (url.includes(StorageKey.ACCESS_TOKEN)) {
      const queryString = url.replace('/#', '');
      const params = Utilities.getQueryParamsFromString(queryString);
      return !!self.processLoginResponse(params.access_token, params.token_type);
    }
  }

  public logout(): void {
    this.storageManager.clearAllStorage();
  }

  private processLoginResponse(token: string, tokenType: string): string {
    if (token == null) {
      throw new Error('Received accessToken was empty');
    }

    this.storageManager.localStorageSetItem(StorageKey.ACCESS_TOKEN, token);
    this.storageManager.localStorageSetItem(StorageKey.TOKEN_TYPE, tokenType);
    return token;
  }
}
