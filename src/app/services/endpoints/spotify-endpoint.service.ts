import { Injectable } from '@angular/core';
import { ConfigurationService } from '../configuration.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { StorageManagerService } from '../storage-manager.service';
import { StorageKey } from 'src/app/constants/storage-key.const';

@Injectable()
export class SpotifyEndpoint {
    constructor(private config: ConfigurationService, private http: HttpClient, private storageManager: StorageManagerService) { }

    public callSpotifyApi<T>(method: string, url: string, data?: any): Promise<T> {
        const self = this;
        return self.http.request<T>(method, self.config.spotifyConfig.apiUrl + url, {
            body: data,
            headers: new HttpHeaders({
                Authorization:
                    self.storageManager.localStorageGetItem(StorageKey.TOKEN_TYPE) +
                    ' ' +
                    self.storageManager.localStorageGetItem(StorageKey.ACCESS_TOKEN)
            })
        }).toPromise<T>();
    }
}
