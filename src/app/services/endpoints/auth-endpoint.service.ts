import { Injectable } from '@angular/core';
import { ConfigurationService } from '../configuration.service';
import { SpotifyAuthConfig } from 'src/app/models/auth.model';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AuthEndpoint {
  constructor(private config: ConfigurationService, private http: HttpClient) { }

  // This is a fake auth endpoint
  public getLoginEndpoint(email: string, password: string): Promise<string> {
    if (email === this.config.fakeCredential.email && password === this.config.fakeCredential.password) {
      return Promise.resolve(this.config.fakeCredential.token);
    } else {
      return Promise.resolve(null);
    }
  }

  public authSpotifyEnpoint(): Promise<any> {
    const self = this;
    const stfConfig: SpotifyAuthConfig = {
      client_id: self.config.spotifyConfig.clientId,
      response_type: self.config.spotifyConfig.responseType,
      redirect_uri: self.config.baseUrl,
      state: '',
      show_dialog: true,
      scope: ''
    };
    return self.http.post(self.config.spotifyConfig.authUrl, stfConfig).toPromise<any>();
  }
}
