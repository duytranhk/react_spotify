import { Injectable } from '@angular/core';
import { UserCredential, SpotifyConfig } from '../models/configuration.model';

@Injectable()
export class ConfigurationService {
  public get baseUrl(): string {
    const port = window.location.port;
    return `${window.location.protocol}//${window.location.hostname}${port && Number(port) !== 80 ? ':' + port : ''}`;
  }

  public get fakeCredential(): UserCredential {
    return {
      email: 'admin@admin.com',
      password: 'admin',
      token: '371f098e-4db9-5fc7-a7c8-6c7ca2800b3c'
    };
  }

  public get spotifyConfig(): SpotifyConfig {
    return {
      clientId: '5d0a902e3f794188a3e203d3d87cd4d4',
      responseType: 'token',
      authUrl: 'https://accounts.spotify.com/authorize',
      apiUrl: 'https://api.spotify.com/v1/'
    };
  }
}
