import { Injectable } from '@angular/core';
import { SpotifyEndpoint } from './endpoints/spotify-endpoint.service';
import { ApiMethod } from '../constants/api-method.const';
import { SpotifyPlaylistModel, SpotifyTrackDetailModel, SpotifyTrackSearchResult } from '../models/spotify.model';

@Injectable()
export class SpotifyService {
    constructor(private spotifyEndpoint: SpotifyEndpoint) {
    }

    public getAustraliaPlaylist(): Promise<SpotifyPlaylistModel> {
        const self = this;
        const id = '37i9dQZEVXbJPcfkRz0wJ0'; // top 50 Australia id
        return self.getPlaylist(id);
    }

    public getPlaylist(id: string): Promise<SpotifyPlaylistModel> {
        const self = this;
        return self.spotifyEndpoint.callSpotifyApi<SpotifyPlaylistModel>(ApiMethod.GET, `playlists/${id}`);
    }

    public getTrack(id: string): Promise<SpotifyTrackDetailModel> {
        const self = this;
        return self.spotifyEndpoint.callSpotifyApi<SpotifyTrackDetailModel>(ApiMethod.GET, `tracks/${id}`);
    }

    public searchTracks(input: string): Promise<SpotifyTrackSearchResult> {
        const self = this;
        return self.spotifyEndpoint.callSpotifyApi<SpotifyTrackSearchResult>(
            ApiMethod.GET,
            `search?q=${encodeURI(input)}&type=track`
        );
    }
}
