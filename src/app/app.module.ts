// Modules
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { NgxAudioPlayerModule } from 'ngx-audio-player';

// Pages
import { AppComponent } from './components/app.component';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { NotFoundComponent } from './components/not-found/not-found.component';

// Service
import { Utilities } from './services/utilities.service';
import { AuthService } from './services/auth.service';
import { AuthEndpoint } from './services/endpoints/auth-endpoint.service';
import { StorageManagerService } from './services/storage-manager.service';
import { ConfigurationService } from './services/configuration.service';
import { SpotifyService } from './services/spotify.service';
import { SpotifyEndpoint } from './services/endpoints/spotify-endpoint.service';

// Controls
import { MenuBarComponent } from './components/controls/menu-bar/menu-bar.component';
import { TopTrackComponent } from './components/controls/top-track/top-track.component';
import { TrackDetailComponent } from './components/controls/track-detail/track-detail.component';
import { SearchTrackComponent } from './components/controls/search-track/search-track.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    NotFoundComponent,
    MenuBarComponent,
    TopTrackComponent,
    TrackDetailComponent,
    SearchTrackComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
    NgxAudioPlayerModule
  ],
  providers: [ConfigurationService, Utilities, StorageManagerService, AuthService, AuthEndpoint, SpotifyService, SpotifyEndpoint],
  bootstrap: [AppComponent]
})
export class AppModule { }
